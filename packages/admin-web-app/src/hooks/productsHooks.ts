import { updateProduct } from "@/services/productsService";
import { useFetcher } from "@/lib/hooks";

export function useProductsSummary() {
	return useFetcher({
		url: "/api/products/summary",
	});
}

export function useProductsList(pageIndex = 1, limit = 10) {
	return useFetcher({
		url: `/api/products?page=${pageIndex}&limit=${limit}`,
	});
}

export function useProductInfo(pid: string | string[]) {
	return useFetcher({
		url: pid && `/api/products/${pid}`,
		mutate: mutate => (id, data) => {
			// update local data
			mutate(draft => ({ ...draft, ...data }), false);
			// update api + refresh
			return mutate(updateProduct(pid, data), false);
		},
	});
}

// ? Add hook to create new product ?
