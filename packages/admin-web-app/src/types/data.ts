export interface ContextValues {
	context: string;
	setContext: (key: string) => void;
}

export interface FormData {
	description: string;
	isVisible: boolean;
	name: string;
	sku: string;
	price: number;
	type: string;
	weight: number;
	categories: number[];
	inventoryLevel: number;
}

export interface TableItem {
	id: number;
	name: string;
	price: number;
	stock: number;
}

export interface ListItem extends FormData {
	id: number;
}

export interface StringKeyValue {
	[key: string]: string;
}
