import fetcher from "@/lib/fetcher";

export const updateProduct = (pid, productData) =>
	fetcher(`/api/products/${pid}`, {
		method: "PUT",
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify(productData),
	});

export const createProduct = productData =>
	fetcher(`/api/products`, {
		method: "POST",
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify(productData),
	});
