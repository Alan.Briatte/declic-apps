import { ErrorProps } from "@/types";

export default async function fetcher(url: string, opts = {}) {
	const res = await fetch(`${url}`, opts);

	// If the status code is not in the range 200-299, throw an error
	if (!res.ok) {
		const { message } = await res.json();
		const error: ErrorProps = new Error(
			message || "An error occurred while fetching the data."
		);
		error.status = res.status; // e.g. 500
		throw error;
	}

	return res.json();
}
