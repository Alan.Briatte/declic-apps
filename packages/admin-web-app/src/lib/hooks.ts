import useSWR from "swr";
import myFetcher from "@/lib/fetcher";

export function useFetcher({
	url,
	fetcher = myFetcher,
	mutate = mutateSwr => mutateSwr,
}: {
	url: string;
	fetcher?: any;
	mutate?: any;
}) {
	const { data, error, mutate: mutateSwr } = useSWR(url, fetcher);

	return {
		data,
		isLoading: !data && !error,
		error,
		mutate: mutate(mutateSwr, url),
	};
}
