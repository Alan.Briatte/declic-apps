import {
	Button,
	Dropdown,
	Small,
	Table,
	Link as StyledLink,
} from "@bigcommerce/big-design";
import { MoreHorizIcon } from "@bigcommerce/big-design-icons";
import Link from "next/link";
import { useRouter } from "next/router";
import ErrorMessage from "@/components/common/Error";
import Loading from "@/components/common/Loading";
import { useProductsList } from "@/hooks/productsHooks";
import { TableItem } from "@/types";
import { useTranslation } from "react-i18next";
import { useEffect, useState } from "react";

const itemsPerPageOptions = [10, 20, 30, 40, 50];

function ProductsList() {
	const { t } = useTranslation();
	const router = useRouter();
	const [currentPage, setCurrentPage] = useState(1);
	const [itemsPerPage, setItemsPerPage] = useState(10);
	const [totalItems, setTotalItems] = useState(0);

	const {
		error,
		isLoading,
		data: { data: list = [], meta: { pagination = false } = {} } = {},
	} = useProductsList(currentPage, itemsPerPage);

	const handleOnPageChange = nextPage => {
		setCurrentPage(nextPage);
	};

	const handleOnItemsPerPageChange = itemsPerPage => {
		setCurrentPage(1);
		setItemsPerPage(itemsPerPage);
	};

	useEffect(() => {
		if (pagination) {
			const { current_page, per_page, total } = pagination;

			setCurrentPage(current_page);
			setItemsPerPage(per_page);
			setTotalItems(total);
		}
	}, [pagination]);

	const tableItems: TableItem[] = list.map(
		({ id, inventory_level: stock, name, price }) => ({
			id,
			name,
			price,
			stock,
		})
	);

	const renderName = (id: number, name: string) => (
		<Link href={`/products/${id}`}>
			<StyledLink>{name}</StyledLink>
		</Link>
	);

	const renderPrice = (price: number): string =>
		new Intl.NumberFormat("en-US", {
			style: "currency",
			currency: "USD",
		}).format(price);

	const renderStock = (stock: number) =>
		stock > 0 ? (
			<Small>{stock}</Small>
		) : (
			<Small bold marginBottom="none" color="danger">
				0
			</Small>
		);

	const renderAction = (id: number) => (
		<Dropdown
			items={[
				{
					content: t("products.editProduct"),
					onItemClick: () => router.push(`/products/${id}`),
					hash: "edit",
				},
			]}
			toggle={
				<Button
					iconOnly={<MoreHorizIcon color="secondary60" />}
					variant="subtle"
				/>
			}
		/>
	);

	if (isLoading) return <Loading />;
	if (error) return <ErrorMessage error={error} />;

	return (
		<Table
			columns={[
				{
					header: t("products.productName"),
					hash: "name",
					render: ({ id, name }) => renderName(id, name),
				},
				{
					header: t("products.stock"),
					hash: "stock",
					render: ({ stock }) => renderStock(stock),
				},
				{
					header: t("products.price"),
					hash: "price",
					render: ({ price }) => renderPrice(price),
				},
				{
					header: t("products.listActions"),
					hideHeader: true,
					hash: "id",
					render: ({ id }) => renderAction(id),
				},
			]}
			items={tableItems}
			itemName={t("products.listItem")}
			stickyHeader
			pagination={{
				currentPage,
				itemsPerPage,
				itemsPerPageOptions,
				onItemsPerPageChange: handleOnItemsPerPageChange,
				onPageChange: handleOnPageChange,
				totalItems,
			}}
		/>
	);
}

export default ProductsList;
