import { Flex, H3, Panel, ProgressCircle } from "@bigcommerce/big-design";
import { useTranslation } from "react-i18next";

const Loading = () => {
	const { t } = useTranslation();
	return (
		<Panel>
			<H3>{t("common.loading")}</H3>
			<Flex justifyContent="center" alignItems="center">
				<ProgressCircle size="large" />
			</Flex>
		</Panel>
	);
};

export default Loading;
