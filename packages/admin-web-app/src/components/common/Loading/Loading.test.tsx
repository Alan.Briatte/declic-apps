import React from "react";
import { render, screen } from "../../../../__tests__/test-utils";
import Loading from "./Loading";

describe("Loading", () => {
	it("should render properly", async () => {
		render(<Loading />);
		const heading = await screen.findByRole("heading");
		expect(heading).toBeInTheDocument();
	});
});
