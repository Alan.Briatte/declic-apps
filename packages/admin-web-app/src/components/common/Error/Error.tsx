import { H3, Panel } from "@bigcommerce/big-design";
import { ErrorMessageProps } from "@/types";
import { useTranslation } from "react-i18next";

const ErrorMessage = ({ error }: ErrorMessageProps) => {
	const { t } = useTranslation();

	return (
		<Panel>
			<H3>{t("common.error")}</H3>
			{error && error.message}
		</Panel>
	);
};
export default ErrorMessage;
