import React from "react";
import { render, screen } from "../../../../__tests__/test-utils";
import Error from "./Error";

import { ErrorMessageProps } from "@/types";

describe("Error", () => {
	it("should render properly", async () => {
		const message = "This is an error";
		const name = "Failed to fetch";
		const error: ErrorMessageProps = {
			error: {
				message,
				name,
			},
		};
		render(<Error error={error.error} />);
		const heading = await screen.findByRole("heading");
		expect(heading).toBeInTheDocument();
	});
});
