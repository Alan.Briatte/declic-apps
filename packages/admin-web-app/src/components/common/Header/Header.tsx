import { Box, Tabs } from "@bigcommerce/big-design";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import i18next from "i18next";

const navigation = [
	{
		id: "home",
		path: "/",
		title: i18next.t("navigation.home"),
	},
	{
		id: "products",
		path: "/products",
		title: i18next.t("navigation.products"),
	},
];

const getRoute = id => {
	const { path } = navigation.find(nav => nav.id === id);
	return path;
};

const Header = () => {
	const { t } = useTranslation();
	const [activeTab, setActiveTab] = useState<string>("");
	const router = useRouter();
	const { pathname } = router;

	useEffect(() => {
		// Check if new route matches navigation
		const currentNav = navigation.find(({ path }) => path === pathname);

		// Set the active tab to tabKey or set no active tab if route doesn't match (404)
		setActiveTab(currentNav?.id ?? "");
	}, [pathname]);

	useEffect(() => {
		// Prefetch products page to reduce latency (doesn't prefetch in dev)
		router.prefetch("/products");
	});

	const handleTabClick = (tabId: string) => {
		setActiveTab(tabId);

		return router.push(getRoute(tabId));
	};

	return (
		<Box marginBottom="xxLarge">
			<Tabs
				activeTab={activeTab}
				items={navigation}
				onTabClick={handleTabClick}
			/>
		</Box>
	);
};

export default Header;
