/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */

import {
	Button,
	Checkbox,
	Flex,
	FormGroup,
	Input,
	Panel,
	Select,
	Form as StyledForm,
	Textarea,
} from "@bigcommerce/big-design";
import { useForm, Controller } from "react-hook-form";
import React from "react";
import { ReactElement } from "react";
import { FormData } from "@/types";
import { useTranslation } from "react-i18next";

interface FormProps {
	formData?: FormData;
	onCancel(): void;
	onSubmit(form: FormData): void;
}

const ProductForm = ({
	onCancel,
	onSubmit,
	formData,
}: FormProps): ReactElement => {
	const { t } = useTranslation();
	const {
		register,
		handleSubmit,
		control,
		formState: { errors },
	} = useForm({
		defaultValues: {
			name: formData ? formData.name : "",
			sku: formData ? formData.sku : "",
			type: formData ? formData.type : "",
			price: formData ? formData.price : "",
			isVisible: formData ? formData.isVisible : false,
			description: formData ? formData.description : "",
			weight: formData ? formData.weight : "",
			categories: formData ? formData.categories : "",
			inventoryLevel: formData ? formData.inventoryLevel : "",
		},
	});

	return (
		<StyledForm onSubmit={handleSubmit(onSubmit)}>
			<Panel header={t("products.basicInformation")}>
				<FormGroup>
					<Input
						error={
							errors.name?.type === "required" && t("common.errorRequired")
						}
						label={t("products.productName").toString()}
						name="name"
						{...register("name", { required: true })}
					/>
				</FormGroup>
				<FormGroup>
					<Input
						error={errors.sku?.type === "required" && t("common.errorRequired")}
						label={t("products.sku").toString()}
						name="name"
						{...register("sku", { required: false })}
					/>
				</FormGroup>
				<FormGroup>
					<Controller
						name="categories"
						control={control}
						render={({ field }) => (
							<SelectRef {...field} label={t("products.productType")} />
						)}
					/>
				</FormGroup>
				<FormGroup>
					<Controller
						name="type"
						control={control}
						render={({ field }) => (
							<SelectRef {...field} label={t("products.productType")} />
						)}
					/>
				</FormGroup>
				<FormGroup>
					<Input
						error={
							errors.price?.type === "required" && t("common.errorRequired")
						}
						iconLeft={"$"}
						label={t("products.defaultPrice").toString()}
						name="price"
						placeholder="10.00"
						type="number"
						step="0.01"
						{...register("price", { required: true })}
					/>
				</FormGroup>
				<FormGroup>
					<Input
						error={
							errors.weight?.type === "required" && t("common.errorRequired")
						}
						iconRight={"KGS"}
						label={t("products.weight").toString()}
						name="weight"
						placeholder="0,00"
						{...register("weight", { required: true })}
					/>
				</FormGroup>
				<FormGroup>
					<Controller
						name="isVisible"
						control={control}
						render={({ field }) => (
							//@ts-ignore
							<Checkbox
								{...field}
								label={t("products.visibleOnStorefront").toString()}
								checked={field.value}
								ref={field.ref}
							/>
						)}
					/>
				</FormGroup>
			</Panel>
			<Panel header={t("products.categories")}>
				<FormGroup>
					<Controller
						name="categories"
						control={control}
						render={({ field }) => (
							<CheckCategories {...field} label={t("products.categories")} />
						)}
					/>
				</FormGroup>
			</Panel>
			<Panel header={t("products.description")}>
				<FormGroup>
					{/* Using description for demo purposes. Consider using a wysiwig instead (e.g. TinyMCE) */}
					<Textarea
						label={t("products.description").toString()}
						name="description"
						placeholder={t("products.productInfo")}
						{...register("description")}
					/>
				</FormGroup>
			</Panel>
			<Flex justifyContent="flex-end">
				<Button
					marginRight="medium"
					type="button"
					variant="subtle"
					onClick={onCancel}
				>
					{t("common.cancel")}
				</Button>
				<Button type="submit">{t("common.submit")}</Button>
			</Flex>
		</StyledForm>
	);
};

export default ProductForm;

//@ts-ignore
const SelectRef = React.forwardRef((props: any, ref) => {
	const { t } = useTranslation();

	return (
		<Select
			options={[
				{ value: "physical", content: t("products.physical") },
				{ value: "digital", content: t("products.digital") },
			]}
			onOptionChange={(v: string) => props.onChange(v)}
			{...props}
		/>
	);
});

const CheckCategories = React.forwardRef((props: any, ref) => {
	const { t } = useTranslation();

	return <></>;
});
