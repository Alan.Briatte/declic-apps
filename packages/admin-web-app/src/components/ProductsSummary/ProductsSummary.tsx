import { Box, Flex, H1, H4 } from "@bigcommerce/big-design";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import ErrorMessage from "@/components/common/Error";
import Loading from "@/components/common/Loading";
import { useProductsSummary } from "@/hooks/productsHooks";

const ProductsSummary = () => {
	const { error, isLoading, data: summary } = useProductsSummary();
	const { t } = useTranslation();

	if (isLoading) return <Loading />;
	if (error) return <ErrorMessage error={error} />;

	return (
		<Flex>
			<StyledBox
				border="box"
				borderRadius="normal"
				marginRight="xLarge"
				padding="medium"
			>
				<H4>{t("summary.inventoryCount")}</H4>
				<H1 marginBottom="none">{summary.inventory_count}</H1>
			</StyledBox>
			<StyledBox
				border="box"
				borderRadius="normal"
				marginRight="xLarge"
				padding="medium"
			>
				<H4>{t("summary.variantCount")}</H4>
				<H1 marginBottom="none">{summary.variant_count}</H1>
			</StyledBox>
			<StyledBox border="box" borderRadius="normal" padding="medium">
				<H4>{t("summary.primaryCategory")}</H4>
				<H1 marginBottom="none">{summary.primary_category_name}</H1>
			</StyledBox>
		</Flex>
	);
};

const StyledBox = styled(Box)`
	min-width: 10rem;
`;

export default ProductsSummary;
