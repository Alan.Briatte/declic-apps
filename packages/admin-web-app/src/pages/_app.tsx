import "../i18n"; // import i18n before all other imports
import { AlertsManager, Box, GlobalStyles } from "@bigcommerce/big-design";
import Header from "@/components/common/Header";
import alertsManager from "@/lib/alertsManager";

function App({ Component, pageProps }) {
	// remove SSR
	return (
		<div suppressHydrationWarning>
			{typeof window === "undefined" ? null : (
				<>
					<GlobalStyles />
					<AlertsManager manager={alertsManager} />
					<Box marginHorizontal="xxxLarge" marginVertical="xxLarge">
						<Header />
						<Component {...pageProps} />
					</Box>
				</>
			)}
		</div>
	);
}
export default App;
