import Head from "next/head";
import { Panel } from "@bigcommerce/big-design";
import ProductsSummary from "@/components/ProductsSummary";
import { useTranslation } from "react-i18next";

export default function IndexPage() {
	const { t } = useTranslation();
	return (
		<main>
			<Head>
				<title>{t("navigation.home")}</title>
			</Head>
			<Panel header={t("summary.title")}>
				<ProductsSummary />
			</Panel>
		</main>
	);
}
