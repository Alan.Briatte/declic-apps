import React from "react";
import ProductForm from "@/components/ProductForm";

const Create = () => {
	const handleSubmit = data => {
		const { sku } = data;

		// le sku doit commencer par au moins 3 lettres et finir par des chiffres
		const reg = /^[a-z]{1,3}[0-9]*$/g;
		if (!sku.match(reg)) {
			alert("Le sku doit commencer par au moins trois lettres");
		}
		console.log("====>", data);
	};

	const onCancel = () => {
		console.log("===> cancel");
	};
	return <ProductForm onSubmit={handleSubmit} onCancel={onCancel} />;
};

export default Create;
