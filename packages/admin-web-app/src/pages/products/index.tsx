import Head from "next/head";
import { useRouter } from "next/router";
import { Panel, Button } from "@bigcommerce/big-design";
import ProductsList from "@/components/ProductsList";
import { useTranslation } from "react-i18next";

export default function Products() {
	const router = useRouter();
	const { t } = useTranslation();

	const handleCreateProduct = () => router.push("/products/create");

	return (
		<main>
			<Head>
				<title>{t("products.productsList")}</title>
			</Head>
			<Panel header={t("products.productsList")}>
				<Button onClick={handleCreateProduct}>
					{t("products.createNewProduct")}
				</Button>
				<ProductsList />
			</Panel>
		</main>
	);
}
