import { useRouter } from "next/router";
import { H1, HR } from "@bigcommerce/big-design";
import ErrorMessage from "@/components/common/Error";
import Loading from "@/components/common/Loading";
import ProductForm from "@/components/ProductForm";
import { useProductInfo } from "@/hooks/productsHooks";
import { FormData } from "@/types";

const ProductInfo = () => {
	const router = useRouter();
	const pid = router?.query?.pid;
	const {
		isLoading,
		data: product,
		error,
		mutate: mutateProduct,
	} = useProductInfo(pid);
	const {
		description,
		is_visible: isVisible,
		name,
		price,
		type,
	} = product ?? {};
	const formData = { description, isVisible, name, price, type };

	const handleCancel = () => router.push("/products");

	const handleSubmit = async (data: FormData) => {
		try {
			const { description, isVisible, name, price, type } = data;
			const apiFormattedData = {
				description,
				is_visible: isVisible,
				name,
				price,
				type,
			};

			await mutateProduct(pid, apiFormattedData);
			return router.push("/products");
		} catch (error) {
			console.error("Error updating the product: ", error);
		}
	};

	if (isLoading) return <Loading />;
	if (error) return <ErrorMessage error={error} />;

	return (
		<>
			<H1>{name}</H1>
			<HR color="secondary30" />
			<ProductForm
				formData={formData}
				onCancel={handleCancel}
				onSubmit={handleSubmit}
			/>
		</>
	);
};

export default ProductInfo;
