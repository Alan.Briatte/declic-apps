/* eslint-disable no-undef */
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { cleanup } from "./__tests__/test-utils";

afterEach(() => {
	jest.clearAllMocks();

	cleanup();
});
