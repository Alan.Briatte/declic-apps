const path = require("path");
module.exports = {
	basePath: process.env.NODE_ENV !== "production" ? "" : "/admin",
	async rewrites() {
		// backend proxy for development
		return process.env.NODE_ENV !== "production"
			? [
					{
						source: "/api/:slug*",
						destination: `http://localhost:${process.env.ADMIN_API_SERVER_PORT}/:slug*`,
						basePath: false,
					},
			  ]
			: [];
	},
	webpack5: true,
	webpack: config => {
		config.resolve.fallback = {
			fs: false,
		};

		config.resolve.alias["handlebars"] = path.resolve(
			__dirname,
			"./node_modules/handlebars/dist/handlebars.js"
		);

		return config;
	},
};
