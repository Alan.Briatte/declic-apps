import Products from "@/pages/products/index";
import { render, screen } from "../../test-utils";

export const ROW_NUMBERS = 5;

// Generate mock tableItems
const generateList = () =>
	[...Array(ROW_NUMBERS)].map((_, index) => ({
		id: index,
		name: `Product ${index}`,
		price: (index + 1) * 10,
		stock: 7,
	}));

jest.mock("@/hooks/productsHooks", () => ({
	useProductsList: jest.fn(() => ({
		error: "",
		isLoading: false,
		data: {
			data: generateList(),
			meta: { pagination: false },
		},
	})),
}));

describe("Product List", () => {
	test("renders correctly", async () => {
		const { container } = render(<Products />);
		// Wait for table to be rendered
		await screen.findByRole("table");

		const rowsLength = screen.getAllByRole("row").length - 1; // Rows - 1 (table header)

		expect(container.firstChild).toMatchSnapshot();

		// Confirm table has been rendered with correct number of rows
		expect(screen.findByRole("table")).toBeDefined();
		expect(rowsLength).toEqual(ROW_NUMBERS);
	});
});
