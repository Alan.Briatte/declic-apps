const sinon = require("sinon");
const expect = require("chai").expect;
const index = require("../src/index");

describe("index", () => {
	it("should handle result", async () => {
		expect(3 + 5).to.equal(8);
	});
});
