const {
	SQSClient,
	ReceiveMessageCommand,
	DeleteMessageCommand,
	Message,
	GetQueueUrlCommand,
} = require("@aws-sdk/client-sqs");

const sqsClient = new SQSClient({});
const cloneEnv = { ...process.env };
const queueName = `ox-app-notification-queue-${cloneEnv.APPLICATION_ID}-${cloneEnv.AWS_SANDBOX_ENVIRONMENT}`;
const poolInterval = cloneEnv.POOL_INTERVAL_IN_MS;
const maxPoolingPhase = cloneEnv.MAX_POOLING_PHASES || 50;
const lambdaLocal = require("lambda-local");
const path = require("path");

console.log(
	"Queue Listening process start for queue => ",
	queueName,
	" start !!! PoolInterval => ",
	poolInterval
);
const params = {
	AttributeNames: ["SentTimestamp"],
	MaxNumberOfMessages: 10,
	MessageAttributeNames: ["All"],
	VisibilityTimeout: 20 /*,
	WaitTimeSeconds: 0,*/,
};
const run = async runCounter => {
	try {
		console.log("Queue Pooling process Start Phase => ", runCounter);
		params.QueueUrl = await getQueueURL();
		const data = await sqsClient.send(new ReceiveMessageCommand(params));
		if (data.Messages) {
			await processMessages(data.Messages);
			var deleteParams = {
				QueueUrl: params.QueueUrl,
				ReceiptHandle: data.Messages[0].ReceiptHandle,
			};
			try {
				const data = await sqsClient.send(
					new DeleteMessageCommand(deleteParams)
				);
				console.log("Message deleted", data);
			} catch (err) {
				console.log("Error", err);
			}
		} else {
			console.log("No messages in the queue");
		}
		console.log("Queue Pooling process is over Phase => ", runCounter);
		return data; // For unit tests.
	} catch (err) {
		console.log("Receive Error", err);
	}
};

async function processMessages(messages) {
	if (messages && messages.length > 0) {
		let records = messages.map(message => {
			let record = {
				awsRegion: "eu-west-1",
				body: message.Body,
				attributes: message.Attributes,
				eventSource: "aws:sqs",
				eventSourceARN: "Not yet  implemented",
				md5OfBody: message.MD5OfBody,
				messageAttributes: message.Attributes,
				messageId: message.MessageId ?? "",
				receiptHandle: message.ReceiptHandle ?? "",
			};
			return record;
		});

		await triggerLambda(records);
		console.log(`${messages.length} messages processing end`);
	}
}
async function getQueueURL() {
	const response = await sqsClient.send(
		new GetQueueUrlCommand({ QueueName: queueName })
	);
	return response.QueueUrl;
}
async function triggerLambda(records) {
	console.log("Lambda execution triggering in", cloneEnv.APPLICATION_ID);
	let event = {
		Records: records,
	};
	lambdaLocal
		.execute({
			event: JSON.parse(JSON.stringify(event)),
			lambdaPath: path.join(__dirname, cloneEnv.WEBHOOK_INVOKE_LAMBDA),
			timeoutMs: 30000,
			verboseLevel: 3
		})
		.then(function (done) {
			console.log("End Lambda execution triggering");
		})
		.catch(function (err) {
			console.error(err);
		});
}
(async function execute() {
	let runCounter = 1;
	var interval = setInterval(async () => {
		runCounter++;
		if (runCounter < maxPoolingPhase) {
			await run(runCounter);
		} else {
			console.log(
				"Max Pooling Phase have been reached. To limit AWS polling and Billing. We unsubscribe !!!"
			);
			clearInterval(interval);
		}
	}, poolInterval);
	//first time
	await run(runCounter);
})();
