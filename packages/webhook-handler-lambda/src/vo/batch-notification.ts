/**
 * Object that is returned within the webhook as part of the notification
 * To avoid to triggers to much sqs messages Webhook gateway gathers all message that have the same type and the same identifier within this Batch Notification object
 * @alias BatchNotification
 * @typedef {object} BatchNotification
 * @property {string} oxatisAppId - Application Identifier/New Oxatis
 * @property {string} storeHash - the store id
 * @property {string} type - the type of notification
 * @property {string} id - the notification id
 * @property {Set<string>} scopes - the list of scopes that have been gathered within this event
 * @property {Array<any>} rawOccurrences - the list of occurrences (Raw Event sent by bigcommerce see   https://developer.bigcommerce.com/docs/ZG9jOjIyMDczNA-webhook-events)
 */
export default interface BatchNotification {
	oxatisAppId: string;
	storeHash: string;
	type: string;
	id: string;
	scopes: Array<string>;
	rawOccurrences: Array<any>;
}
