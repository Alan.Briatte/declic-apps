export default class CustomError extends Error {
	constructor(
		public class_name: string,
		public method_name: string,
		message: string
	) {
		super(message);
		Object.setPrototypeOf(this, CustomError.prototype);
		this.class_name = class_name;
		this.method_name = method_name;
	}
}
