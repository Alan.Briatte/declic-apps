/**
 * Here we will handle routes
 */
import { CustomError } from "./errors";
import { SQSEvent } from "aws-lambda";
import BatchNotification from "./vo/batch-notification";
exports = module.exports;
exports.handler = async (event: SQSEvent): Promise<any> => {
	try {
		const datas = [];
		for (let i = 0; i < event.Records.length; i++) {
			const data: BatchNotification = JSON.parse(event.Records[i].body); //you should create a typescript object
			console.log("Daata received ", JSON.stringify(data));
			datas.push(data);
		}
		//Return is not mandatory here
		return datas;
	} catch (err) {
		console.log("An error occured ", err);
		throw new CustomError(
			"index",
			"handler",
			"Failed to execute. contact an administrator"
		);
	}
};
