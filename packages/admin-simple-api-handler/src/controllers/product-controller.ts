import {
	GET,
	POST,
	PUT,
	DELETE,
	Controller,
	ResponseWrapper,
} from "@oxatis-bigcommerce/lambda-router";
import { BigCommerceClientV2 } from "@oxatis-bigcommerce/api-client";
import { RequestWrapper } from "@oxatis-bigcommerce/lambda-router/dist/vo";
import { productService } from "../services";
import { Product } from "../vo";

const DEFAULT_PAGE = "1";
const DEFAULT_LIMIT = "10";
@Controller("/products")
export default class ProductController {
	@GET("")
	async getProducts(
		request: RequestWrapper
	): Promise<ResponseWrapper | Array<Product>> {
		const storeHash = request.securityContext.storeHash;
		const page = parseInt(request.getQueryParameter("page") || DEFAULT_PAGE);
		const limit = parseInt(request.getQueryParameter("limit") || DEFAULT_LIMIT);
		return productService.getProducts(storeHash, page, limit);
	}

	@GET("/summary")
	async getSummary(
		request: RequestWrapper
	): Promise<ResponseWrapper | boolean> {
		//Store Hash will be always provided as part of the security context
		const storeHash = request.securityContext.storeHash;
		return productService.getSummary(storeHash);
	}

	@GET("/{productId}")
	async getProduct(request: RequestWrapper): Promise<ResponseWrapper | any> {
		//Store Hash will be always provided as part of the security context
		const storeHash = request.securityContext.storeHash;
		//Path parameter can be securely retrieved  from the PathParameter Method
		//i.e. There is also a getQueryParameter method
		const productId = request.getPathParameter("productId");
		//Responsewrapper allow us to send typed error
		if (!productId) {
			return {
				statusCode: 400,
				body: "ProductId is mandatory for getProduct",
			};
		}
		return productService.getProduct(storeHash, productId);
	}

	@POST("")
	async createProduct(request: RequestWrapper): Promise<ResponseWrapper> {
		//ResponseWrapper could also be used to send a status
		return {
			statusCode: 501,
			body: "Not yet Implemented",
		};
	}

	@PUT("/{productId}")
	async updateProduct(request: RequestWrapper): Promise<ResponseWrapper | any> {
		console.log("update");
		const storeHash = request.securityContext.storeHash;
		const productId = request.getPathParameter("productId");
		if (!productId) {
			return {
				statusCode: 400,
				body: "ProductId is mandatory for updateProduct",
			};
		}
		/**
		 * RequestWrapper payload is a json parsed if application/json
		 */
		return productService.updateProduct(storeHash, productId, request.payload);
	}

	@DELETE("/{productId}")
	async deleteProduct(request: RequestWrapper): Promise<ResponseWrapper> {
		//ResponseWrapper could also be used to send a status
		return {
			statusCode: 501,
			body: "Not yet Implmemented",
		};
	}
}
