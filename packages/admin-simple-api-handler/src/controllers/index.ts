/**
 * Controllers are the entrypoint for the API Request
 */
import ProductController from "./product-controller";
export { ProductController };
