import "reflect-metadata";
import {
	APIGatewayEvent,
	Context,
	Callback,
	APIGatewayProxyResult,
} from "aws-lambda";
import { RoutingFactory } from "@oxatis-bigcommerce/lambda-router";
import { ProductController } from "./controllers";

/**
 * Here we add globally controller in order for them to be added one time for the lambda instance
 */
RoutingFactory.addController(ProductController);

exports = module.exports;
exports.handler = async (
	event: APIGatewayEvent,
	context: Context,
	callback: Callback<APIGatewayProxyResult>
): Promise<any> => {
	console.log("test gitlab ");
	return await RoutingFactory.route(event, context, callback);
};
