/**
 * Services mainly manage all business aspects . They can be called by different controllers
 */
import { productService } from "./product-services";
export { productService };
