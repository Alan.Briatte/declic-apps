import { Product } from "../vo";
import { BigCommerceClientV2 } from "@oxatis-bigcommerce/api-client";

class ProductServices {
	/**
	 * Retrieves Produt
	 * @param storeHash
	 */
	async getProducts(
		storeHash: string,
		page: number,
		limit: number
	): Promise<Array<Product>> {
		const data = await BigCommerceClientV2.getInstance().get({
			storeHash,
			path: `/catalog/products?page=${page}&limit=${limit}`,
		});
		return data;
	}

	async getProduct(storeHash: string, productId: string): Promise<any> {
		const { data } = await BigCommerceClientV2.getInstance().get({
			storeHash,
			path: `/catalog/products/${productId}`,
		});
		return data;
	}
	async getSummary(storeHash: string): Promise<any> {
		const { data } = await BigCommerceClientV2.getInstance().get({
			storeHash,
			path: "/catalog/summary",
		});
		return data;
	}

	async updateProduct(
		storeHash: string,
		productId: string,
		product: any
	): Promise<any> {
		const { data } = await BigCommerceClientV2.getInstance().put({
			storeHash,
			path: `/catalog/products/${productId}`,
			data: product,
		});
		return data;
	}
}
const productService = new ProductServices();
export { productService };
