export default class MandatoryParameterError extends Error {
	constructor(
		public class_name: string,
		public method_name: string,
		public field_name: string,
		public message: string
	) {
		super(message);
		Object.setPrototypeOf(this, MandatoryParameterError.prototype);
		this.name = "MandatoryParameterException";
		this.class_name = class_name;
		this.method_name = method_name;
		this.field_name = field_name;
	}
}
