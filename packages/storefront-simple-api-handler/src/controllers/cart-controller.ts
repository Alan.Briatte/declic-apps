import {
	Controller,
	GET,
	POST,
	ResponseWrapper,
} from "@oxatis-bigcommerce/lambda-router";
import { RequestWrapper } from "@oxatis-bigcommerce/lambda-router/dist/vo";
import { cartService } from "../services";
import { Cart } from "../vo/cart";

@Controller("/carts")
export default class CartController {
	@POST("")
	async create(request: RequestWrapper): Promise<ResponseWrapper | Cart> {
		//Store Hash will be always provided as part of the security context
		const storeHash = request.securityContext.storeHash;
		return cartService.create(storeHash, request.payload);
	}

	/**
	 *
	 */
	@GET("/{cartId}")
	async getCarts(request: RequestWrapper): Promise<ResponseWrapper | Cart> {
		//Store Hash will be always provided as part of the security context
		const storeHash = request.securityContext.storeHash;
		//Path parameter can be securely retrieved  from the PathParameter Method
		//i.e. There is also a getQueryParameter method
		const cartId = request.getPathParameter("cartId");
		//Responsewrapper allow us to send typed error
		if (!cartId) {
			return {
				statusCode: 400,
				body: "cartId is a mandatory parameter",
			};
		}
		console.log("Cart find By Id");
		//If only payload is sent Router assume that it is status 200
		return cartService.findCartById(storeHash, cartId);
	}
}
