import { Cart } from "../vo/cart";
import {
	BigCommerceApiVersion,
	BigCommerceClientV2,
} from "@oxatis-bigcommerce/api-client";

class CartService {
	async findCartById(storeHash: string, cartId: string): Promise<Cart> {
		const cart = (await BigCommerceClientV2.getInstance().get({
			storeHash,
			path: `/carts/${cartId}`,
		})) as Cart;
		return cart;
	}

	async create(storeHash: string, data: any): Promise<Cart> {
		const cart = (await BigCommerceClientV2.getInstance().post({
			storeHash,
			path: `/carts`,
			apiVersion: BigCommerceApiVersion.V3,
			data: data,
		})) as Cart;
		return cart;
	}
}
const cartService = new CartService();
export { cartService };
