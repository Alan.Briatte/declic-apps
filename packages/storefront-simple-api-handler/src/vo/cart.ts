export interface Cart {
	id: string;
	parent_id?: string;
	customer_id?: string;
	email: string;
}
