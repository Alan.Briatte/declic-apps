/**
 * This is used only to triggers dev server with express
 */

const express = require("express");
import { initialize } from "express-openapi";
const lambdaLocal = require("lambda-local");
const path = require("path");
const app = express();
const port = process.env.ADMIN_API_SERVER_PORT;
const cors = require("cors");

import YAML from "yaml";
import fs from "fs";
var bodyParser = require("body-parser");
console.log("API File reading and parsing start");
const file = fs.readFileSync("./api.yaml", "utf8");
const apiDocs = YAML.parse(file);
console.log("API File reading and parsing done");
console.log("Operation Mock generation start");
const operations = {};
for (const [apiPath, apiPathDefinition] of Object.entries(apiDocs.paths)) {
	for (const [operationKey, operationDefinition] of Object.entries(
		apiPathDefinition
	)) {
		operations[operationDefinition.operationId] = function (req, res) {
			res
				.send("Not Implemented, it should have been intercepted")
				.statusCode(501);
		};
	}
}
console.log("Operation Mock generation is done");
app.use(cors());
initialize({
	app,
	apiDoc: {
		...apiDocs,
		"x-express-openapi-additional-middleware": [requestInterceptor],
	},
	consumesMiddleware: {
		"application/json": bodyParser.json(),
		"text/text": bodyParser.text(),
	},
	promiseMode: true,
	operations: operations,
	errorMiddleware: function (err, req, res, next) {
		// only handles errors for /v3/*
		/* do something with err in a v3 way */
		console.log("An error occured ", err);
		res.status(err.status).send(err.errors);
	},
});
app.listen(port, function () {
	console.log(`Enjoy your local Admin API at http://localhost:${port}`);
});
function requestInterceptor(req, res, next) {
	let bodystring = req.body ? JSON.stringify(req.body) : "";
	/**
	 * We replace here the semi colon for the path management
	 */
	let colonFound = false;
	let path = req.route.path;
	for (var i = 0; i < path.length; i++) {
		var character = path[i];
		if (character === ":") {
			let replacement = "{";
			path =
				path.substr(0, i) + replacement + path.substr(i + replacement.length);
			colonFound = true;
		} else if (character === "/" && colonFound) {
			let replacement = "|";
			path =
				path.substr(0, i) + replacement + path.substr(i + replacement.length);
			colonFound = false;
		}
	}
	path = path.replace(/\|/g, "}/");
	if (colonFound) {
		path = path + "}";
	}
	/**
	 * End Colon Management
	 */
	let event = {
		httpMethod: req.method,
		resource: path,
		path: req.path,
		queryStringParameters: req.query,
		body: bodystring,
		pathParameters: req.params,
		headers: req.headers,
	};
	routeEvent(event, res);
}

/**
 * Here we route the event to lambda local
 * @param event
 */
function routeEvent(event, res) {
	event.requestContext = {
		authorizer: {
			storeHash: process.env.STORE_HASH,
		},
	};
	lambdaLocal
		.execute({
			event: event,
			lambdaPath: path.join(__dirname, process.env.ADMIN_INVOKE_LAMBDA),
			timeoutMs: 30000,
			verboseLevel: 3
		})
		.then(function (data) {
			res.header("content-type", data.headers["Content-Type"]);
			res.status(data.statusCode).send(data.body);
		})
		.catch(function (err) {
			console.log(err);
		});
}
