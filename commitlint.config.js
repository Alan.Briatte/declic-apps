const {
	utils: { getPackages },
} = require("@commitlint/config-lerna-scopes");

module.exports = {
	extends: [
		"@commitlint/config-conventional",
		"@commitlint/config-lerna-scopes",
	],
	rules: {
		"scope-enum": async ctx => {
			return [2, "always", [...(await getPackages(ctx)), "release", "global"]];
		},
	},
};
