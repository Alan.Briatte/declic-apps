# Oxatis NodeJS Template for Apps Building

## Introduction

Apps Boilerplate in its first version before CLI implementation will be a
skeleton that will allow Oxatis Apps developper to initialize an apps Skeleton
with all capabilities needed to implement them.

- Admin Behaviour Implementation ( Backend & Frontend)
- Storefront Behaviour Implementation ( Backend & Frontend)
- Webhook Implementation
- Local Development
- Deployment through ALM & Oxatis CI/CD

## Description

The project is itself a mono-project based on lerna that have natively the
following structure (only important file have been mentionned here) :

- ├── **infrastructure**
- │ ├── apps_backbone*.tf (*Terraform file that are used to deploy Apps Backbone
  Architecture i.e. Web exposition, token management\*)
- │ ├── lambda\_*.tf (*terraform file used to deployb lambda i.e. Webhook,
  Admin, Storefront\*)
- | ├── config
- │ │ ├── local_generation.sh.template (Template file used to generate Terraform
  default file for local execution purpose)
- │ │ ├── terraform.tfvars.json.template (JSON Template that will be filled by
  CI/CD and passed as parameters files)
- ├── **packages**
- | ├── admin-simple-api-handler _API Sample Implementation for the Admin_
- | ├── admin-web-app _Web Application Sample Implementation for the Admin_
- | ├── storefront-simple-api-handler _API Implementation for the Storefront_
- | ├── webhook-handler-lambda _Webhook Handler_
- ├── **resources**
- | ├── admin-api _API Sample Implementation for the Admin_
- | ├── storefront-api _API Sample Implementation for the Admin_
- ├──.dev-env.template (\* template file used for Local Development )
- ├──gitlab-config.yml (_YAML file that provides Property that we will need to
  deployed the project accross environments_)
- ├──.gitlab-ci.yml (**Gitlab file DO NOT MODIFY IT**)
- ├── package.json
- └── .gitignore

## Usage

### Project Initialization

1. Clone it in an empty repository
2. Update the gitlab-config.yml with your apps Identifier particularly the
   following :
   - PROJECT_NAME
   - APPLICATION_ID
   - APPLICATION_DESCRIPTION
3. Start your Implementation
4. Update Readme.md

### Local Implementation

The local Implementation is based on emulation of AWS services on local. It is
not perfect in terms of features but we tend to be close as possible from our
needs.

1. Copy the .dev-env.template file and rename it to .dev-env
2. Update the properties with your local environments settings
   - ACCESS_TOKEN (Static Access Token generated within Bigcommerce Admin)
   - APPLICATION_ID (Application Identifier filled within the gitlab-config.yml)
   - STORE_HASH (your Bigcommerce StoreHash)
   - AWS_SANDBOX_ENVIRONMENT
3. The "start:dev" script trigger at the root of the project will create a
   consistent dev environment with the following capabilities :
   - Admin Web Application + Admin API
   - Storefront API
   - Webhook Lambda (that will poll the sqs lambda associated to the
     Application_id within the subsequent aws connection)

_NB_:

- Webhook Lambda Implementation will need an AWS Connection a queue created
  before its usage
- All scripts execution can be debbuggued within any IDE
