## Execution
CI/CD Pipeline orchestrates infrastructure creation by executing the following :

- Variable replacement
Calling the dpeloyment tools that will be integrated in a Docker Image
- Terraform validate
- Terraform apply with a configuration Variables
**terraform apply -var-file="config/terraform.tfvars.json"**

##Variables Replacement

End user will have to do the following for its variables to be integrated :
- Add the variable name in config/config_variables.sh (both as variable and in the variable Name)
- write there replacement in terraform.tfvars.json.template on the format @XXX@
	- **@AWS_REGION@**

 