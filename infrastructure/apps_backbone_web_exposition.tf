/**
* This component is in chrage of deploying Web resources S3 , Cloudfront and API Gateway
*/
/**
* This module deploys apps web resources (API,web app, Cloudfront) in generic and secure ways
* Only the following should be updated :
    - openapi_template
    - open_api_data to be injected
    -
*/

variable "admin_web_configuration" {
  type = object({
    api_specs_location= string
    webapp_location= string
    api_deployment_enabled = bool
  })
}
variable "storefront_web_configuration" {
  type = object({
    api_specs_location= string
    webapp_location= string
    api_deployment_enabled = bool
    web_deployment_enabled = bool
  })
}


module "apps_web_component" {
  /**
  * Generic COnfiguration that should not be updated
  */
  source                  = "gitlab.com/new-oxatis1/apps-web-exposition/local"
  version                 = "2.5.1"
  application_id          = local.application_id
  cdn_dns_certificate_arn = local.acm_certificate_arn_us
  domain_prefix           = local.domain_prefix
  environment             = local.environment
  hosted_zone             = local.hosted_zone
  /**
  * Project Configuration that can be updated
  */
  admin_api_configuration = {
    openapi_template = var.admin_web_configuration.api_specs_location
    open_api_data = {
      business_lambda_arn : local.admin_lambda_invoke_arn
    }
    enabled =var.admin_web_configuration.api_deployment_enabled
  }
  storefront_api_configuration = {
    openapi_template = var.storefront_web_configuration.api_specs_location
    open_api_data = {
      business_lambda_arn : local.storefront_lambda_invoke_arn
    }
    enabled =var.storefront_web_configuration.api_deployment_enabled
  }
  cdn_web_app_ttl = {
    min_ttl     = var.cdn_web_min_ttl
    default_ttl = var.cdn_web_default_ttl
    max_ttl     = var.cdn_web_max_ttl
  }
  cdn_api_ttl = {
    min_ttl     = var.cdn_api_min_ttl
    default_ttl = var.cdn_api_default_ttl
    max_ttl     = var.cdn_api_max_ttl
  }
  public_scripts = "${local.build_directory}/public"
  admin_webapp_files_build       = "${local.build_directory}/${var.admin_web_configuration.webapp_location}"
  cloudwatch_logs_retention_days = var.default_cloudwatch_logs_retention_in_days
}
