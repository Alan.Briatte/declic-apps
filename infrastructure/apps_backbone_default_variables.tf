/**
* Here are listed set opf default variables that are deployed as part of CI/CD process
* DO NOT MODIFY IT
*/
variable "default_cloudwatch_logs_retention_in_days" {
  type = number
}

variable "admin_authorizer_cache_ttl" {
  type = number
}

variable "storefront_authorizer_cache_ttl" {
  type = number
}

variable "cdn_web_min_ttl" {
  type = number
}

variable "cdn_web_max_ttl" {
  type = number
}

variable "cdn_web_default_ttl" {
  type = number
}

variable "cdn_api_min_ttl" {
  type = number
}

variable "cdn_api_max_ttl" {
  type = number
}

variable "cdn_api_default_ttl" {
  type = number
}