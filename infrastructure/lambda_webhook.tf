variable "webhook_handler_reserved_concurrency_exec" {
  type = number
}
variable "webhook_enabled_in_sandbox" {
  type = bool
  description = "To know if webhook should be enabled when it is deployed within a sandbox (non prod environment). It allows to switch between local Vs Sandbox mode"
  default = false
}
variable "webhook_lambda_configuration" {
  type = object({
    function_name = string
    function_description = string
    runtime = string
    memory_size= number
    timeout_seconds= number
    archive_name = string
  })
}

locals {
  webhook_lambda_archive_location = "${local.lambda_archive_prefix}/${var.webhook_lambda_configuration.archive_name}"
  non_sandbox_environment=["dev","development","qa","preproduction","production"]
  //Enabled if environment =prod, dev,preprod, qa or enabled by a developer action in sandbox
  webhook_listener_enabled= contains(local.non_sandbox_environment,local.environment)|| var.webhook_enabled_in_sandbox
}

module "webhook_lambda" {
  source = "./modules/apps_lambda"
  application_id = local.application_id
  environment = local.environment
  archive_location = local.webhook_lambda_archive_location
  function_name = var.webhook_lambda_configuration.function_name
  function_description = var.webhook_lambda_configuration.function_description
  runtime = var.webhook_lambda_configuration.runtime
  memory_size=var.webhook_lambda_configuration.memory_size
  logs_retention_in_days = var.default_cloudwatch_logs_retention_in_days
  timeout_seconds =var.webhook_lambda_configuration.timeout_seconds
  token_reader_policy= module.apps_foundation.token_reader_policy
  reserved_concurrent_executions = var.webhook_handler_reserved_concurrency_exec
  archive_bucket = local.deployment_bucket
  inline_policies = {
    //Permissions to receive messagse from webhook entrypoint
    sqs_webhook_dispatcher_receiveMessage = {
      name = "webhook_handler_receive_message"
      policy = jsonencode(
        {
          "Version" = "2012-10-17",
          "Statement" = [
            {
              Action = [
                "sqs:ReceiveMessage",
                "sqs:DeleteMessage",
                "sqs:GetQueueAttributes"
              ],
              Effect   = "Allow",
              Resource = module.apps_foundation.notification_queue_arn
            }
          ]
        }
      )
    }
  }
}

resource "aws_lambda_event_source_mapping" "event_source_mapping" {
  event_source_arn = module.apps_foundation.notification_queue_arn
  enabled          =  local.webhook_listener_enabled
  function_name    = module.webhook_lambda.lambda_arn
  // Batch size is important for performance and cost do not put one to have single retrieving
  batch_size = 10
}
