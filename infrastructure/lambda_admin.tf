variable "admin_lambda_configuration" {
  type = object({
    function_name = string
    function_description = string
    runtime = string
    memory_size= number
    timeout_seconds= number
    archive_name = string
  })
}

locals {
  admin_lambda_archive_location      = "${local.lambda_archive_prefix}/${var.admin_lambda_configuration.archive_name}"
  admin_lambda_enabled=var.admin_web_configuration.api_deployment_enabled
  admin_lambda_invoke_arn=local.admin_lambda_enabled? module.lambda_admin[0].lambda_invoke_arn:""
}

module "lambda_admin" {
  count = local.admin_lambda_enabled?1:0
  source = "./modules/apps_lambda"
  application_id = local.application_id
  environment = local.environment
  archive_location = local.admin_lambda_archive_location
  function_name = var.admin_lambda_configuration.function_name
  function_description = var.admin_lambda_configuration.function_description
  runtime = var.admin_lambda_configuration.runtime
  memory_size=var.admin_lambda_configuration.memory_size
  logs_retention_in_days = var.default_cloudwatch_logs_retention_in_days
  timeout_seconds =var.admin_lambda_configuration.timeout_seconds
  token_reader_policy= module.apps_foundation.token_reader_policy
  archive_bucket = local.deployment_bucket
}


resource "aws_lambda_permission" "apigw_admin_lambda_permissions_invoke" {
  count = local.admin_lambda_enabled?1:0
  statement_id  = "AllowExecutionFromAdmin-${local.application_id}-API"
  action        = "lambda:InvokeFunction"
  function_name =module.lambda_admin[0].lambda_arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = module.apps_web_component.admin_api_stage_execution_arn
}
