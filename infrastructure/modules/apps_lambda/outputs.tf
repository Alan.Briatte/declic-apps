output "lambda_arn" {
    value=aws_lambda_function.apps_lambda.arn
}

output "lambda_invoke_arn" {
    value=aws_lambda_function.apps_lambda.invoke_arn
}


output "lambda_role_arn" {
    value=aws_iam_role.apps_lambda_lambda_role.arn
}

output "lambda_role_name" {
    value=aws_iam_role.apps_lambda_lambda_role.name
}