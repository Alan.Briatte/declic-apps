variable "application_id" {
  type = string
}
variable "function_name" {
  type = string
  description = "Function name"
}
variable "function_description" {
  type = string
  description = "Function description"
  default = ""
}


variable "handler" {
  type = string
  description = "handler"
  default = "index.handler"
}
variable "memory_size" {
  type=number
  description="The memory_size of the Lambda function"
}
variable "runtime" {
  type=string
  description="The runtime of the Lambda function"
}
variable "timeout_seconds" {
  type=number
  description="The timeout_seconds of the Lambda function"
  default = 30
}


variable "managed_policies" {
  description = "Managed Policies"
  type        = list
  default =[]
}

variable "inline_policies"{
  description = "Inline  Policies"
  type        = map(object({
    name  = string
    policy= string
  }))
  default={}
}


variable "reserved_concurrent_executions" {
  type=number
  description="Amount of reserved concurrent executions for this lambda function. A value of 0 disables lambda from being triggered and -1 removes any concurrency limitations. Defaults to Unreserved Concurrency Limits -1"
  default =-1
}

variable "environment" {
  description = "Environment where the lambda is deployed"
  type = string
}

variable "environment_variables" {
  description = "Environment Variables"
  type        = map(string)
  default={}
}

variable "logs_retention_in_days" {
  type = string
}

variable "token_reader_policy" {
  type = string
}

variable "archive_bucket" {
  type = string
}
variable "archive_location" {
  description = "Where are the lambda artefact"
  type        = string
}
