

locals{
    default_env_variables={
      "ENVIRONMENT":var.environment
      "APPLICATION_ID" : var.application_id
    }
  computed_function_name="${var.application_id}_${var.function_name}"
}
data "aws_s3_bucket_object" "lambda_code" {
  bucket = var.archive_bucket
  key = var.archive_location
}

resource "aws_lambda_function" "apps_lambda" {
  function_name                   = local.computed_function_name
  description                     = var.function_description
  handler                         = var.handler
  role                            = aws_iam_role.apps_lambda_lambda_role.arn
  memory_size                     = var.memory_size
  s3_bucket = data.aws_s3_bucket_object.lambda_code.bucket
  s3_key = data.aws_s3_bucket_object.lambda_code.key
  source_code_hash=data.aws_s3_bucket_object.lambda_code.etag
  runtime                         = var.runtime
  timeout                         = var.timeout_seconds
  reserved_concurrent_executions  = var.reserved_concurrent_executions
  environment {
      variables                   = merge(var.environment_variables,local.default_env_variables)
  }
  depends_on                      = [
    aws_cloudwatch_log_group.lambda_log_group,
    aws_iam_role_policy_attachment.managed_policies_attachment
  ]
}
