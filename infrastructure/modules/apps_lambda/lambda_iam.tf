resource "aws_iam_role" "apps_lambda_lambda_role" {
  name        = "${local.computed_function_name}_role"
  description = "${local.computed_function_name} dedicated role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "managed_policies_attachment" {
  for_each = toset(var.managed_policies)
   policy_arn = each.key
  role       = aws_iam_role.apps_lambda_lambda_role.name
}

resource "aws_iam_role_policy" "inline_policies_attachment" {
  for_each = var.inline_policies
  name = each.value.name
  role = aws_iam_role.apps_lambda_lambda_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = each.value.policy
}


resource "aws_iam_role_policy_attachment" "basic_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role       = aws_iam_role.apps_lambda_lambda_role.name
}

/**
* Mandatory for token reading
*/

resource "aws_iam_role_policy_attachment" "lambda_token_reader_attach" {
  policy_arn = var.token_reader_policy
  role       = aws_iam_role.apps_lambda_lambda_role.id
}