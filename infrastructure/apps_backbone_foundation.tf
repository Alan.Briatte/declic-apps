/**
* This module deploys apps backbone (Access Token  , ssm to store clientid client secret ...)
* It should not be updated
*/
module "apps_foundation" {
  source         = "gitlab.com/new-oxatis1/apps-backbone/local"
  version        = "1.4.2"
  application_id = local.application_id
  environment    = local.environment
  hosted_zone    = local.hosted_zone
}
