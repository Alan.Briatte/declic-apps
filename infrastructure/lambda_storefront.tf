variable "storefront_lambda_configuration" {
  type = object({
    function_name = string
    function_description = string
    runtime = string
    memory_size= number
    timeout_seconds= number
    archive_name = string
  })
}

locals {
  storefront_lambda_archive_location      = "${local.lambda_archive_prefix}/${var.storefront_lambda_configuration.archive_name}"
  storefront_lambda_enabled=var.storefront_web_configuration.api_deployment_enabled
  storefront_lambda_invoke_arn = local.storefront_lambda_enabled?module.lambda_storefront[0].lambda_invoke_arn:""
}

module "lambda_storefront" {
  count = local.storefront_lambda_enabled?1:0
  source = "./modules/apps_lambda"
  application_id = local.application_id
  environment = local.environment
  archive_location = local.storefront_lambda_archive_location
  function_name = var.storefront_lambda_configuration.function_name
  function_description = var.storefront_lambda_configuration.function_description
  runtime = var.storefront_lambda_configuration.runtime
  memory_size=var.storefront_lambda_configuration.memory_size
  logs_retention_in_days = var.default_cloudwatch_logs_retention_in_days
  timeout_seconds =var.storefront_lambda_configuration.timeout_seconds
  token_reader_policy= module.apps_foundation.token_reader_policy
  archive_bucket = local.deployment_bucket
}
resource "aws_lambda_permission" "apigw_storefront_lambda_permissions_invoke" {
  count = local.storefront_lambda_enabled?1:0
  statement_id  = "AllowExecutionFromStorefront-${local.application_id}-API"
  action        = "lambda:InvokeFunction"
  function_name =module.lambda_storefront[0].lambda_arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = module.apps_web_component.storefront_api_stage_execution_arn
}
