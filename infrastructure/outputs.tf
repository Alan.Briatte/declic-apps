output "admin_url"{
  value= module.apps_web_component.admin_url
}
output "admin_api_url"{
  value= module.apps_web_component.admin_api_url
}

output "storefront_url" {
  value = module.apps_web_component.storefront_url
}


output "storefront_api_url" {
  value = module.apps_web_component.storefront_api_url
}

output "web_bucket_name" {
  value = module.apps_web_component.web_bucket_name
}