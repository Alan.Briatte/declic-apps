
import svelte from "rollup-plugin-svelte";
import multiInputObj from "rollup-plugin-multi-input";
import resolve from "@rollup/plugin-node-resolve";
import { terser } from "rollup-plugin-terser";

const multiInput = multiInputObj.default;

export default [
	{
		input: "./src/webComponents/*.js",
		output: {
			format: "esm",
			dir: "build",
		},
		plugins: [
			multiInput({ relative: "src/webComponents/" }),
			svelte({
				exclude: /\.wc\.svelte$/,
				compilerOptions: {
					// enable run-time checks when not in production
					dev: false,
					customElement: false,
					enableSourcemap: false,
				},
				emitCss: false,
			}),
			svelte({
				// compile only *.wc.svelte files as web components
				include: /\.wc\.svelte$/,
				compilerOptions: {
					// enable run-time checks when not in production
					dev: false,
					customElement: true,
					enableSourcemap: false,
				},
				emitCss: true,
			}),
			resolve(),
			terser(),
		]
	},
	{
		input: "src/page/*.js",
		output: {
			format: "iife",
			dir: "build",
		},
		plugins: [
			multiInput({ relative: "src/page/" }),
			resolve(),
			terser(),
		]
	},
];
