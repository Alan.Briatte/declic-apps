module.exports = {
	scripts: [
		{
			id: "storefront",
			name: "@APPLICATION_ID@-storefront-script",
			description: "Lorem Ipsum",
			auto_uninstall: true,
			load_method: "default",
			location: "head",
			visibility: "storefront", //storefront, all_pages, checkout, order_confirmation
			kind: "script_tag",
			consent_category: "essential", //essential, functional, analytics, targeting
			enabled: true,
			html: `
   				<script defer src="@storefront_public_url@/storefront-script/index.js"></script>
   				<script type="module" src="@storefront_public_url@/storefront-script/HelloWorld.js"></script>
			`,
		},
	],
};
