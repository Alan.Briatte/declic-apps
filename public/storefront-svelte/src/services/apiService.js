import { getPageConfig } from "./helpers";

const token = getPageConfig("storefront_api_token");
const STORE_HASH = getPageConfig("store_hash");
const STOREFRONT_URL = getPageConfig("secure_url");
const API_URL = getPageConfig("storefront_api_url");

/**
 * Request SDK.
 * Wil aad header to authentified by the storefront-api
 * @param method
 * @param path
 * @param body
 * @return {Promise<any>}
 */
export const send = ({ method = "GET", path = "/infos", body = null }) => {
	const request = {
		method,
		headers: {
			"Content-Type": "application/json",
			oxAuthorization: token,
			STORE_HASH,
			STOREFRONT_URL,
		},
	};
	if (body) {
		request.body = JSON.stringify(body);
	}

	return fetch(`${API_URL}${path}`, request).then(res => res.json());
};
