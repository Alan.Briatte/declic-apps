import { APP_NAME } from "../constants";

/**
 * Gets params from page
 * @param key - param from the window.oxatis object to fetch
 * @param defaultValue - defautl value
 * @return {*|string}
 */
export const getPageConfig = (key, defaultValue = "") => {
	const config = window?.oxatis[APP_NAME] || {};
	return typeof config[key] !== "undefined" ? config[key] : defaultValue;
};

export const formatCurrency = ({
	price = 0,
	currency_location = getPageConfig("currency_location", "right"),
	currency_token = getPageConfig("currency_token", "€"),
	decimal_places = getPageConfig("decimal_places", 2),
	thousands_token = getPageConfig("thousands_token", " "),
	decimal_token = getPageConfig("decimal_token", ","),
} = {}) => {
	if (typeof price !== "number") return;

	let priceText = price.toLocaleString("fr-FR", {
		minimumFractionDigits: decimal_places,
		maximumFractionDigits: decimal_places,
	});
	priceText = priceText.replace(/\s/, thousands_token);
	priceText = priceText.replace(/,/, decimal_token);

	priceText =
		currency_location === "left"
			? currency_token + priceText
			: priceText + currency_token;

	return priceText;
};
