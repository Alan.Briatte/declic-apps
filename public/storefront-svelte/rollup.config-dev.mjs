import svelte from "rollup-plugin-svelte";
import multiInputer from "rollup-plugin-multi-input";
import resolve from "@rollup/plugin-node-resolve";
import summary from "rollup-plugin-summary";
import fs from "fs";
import { spawn } from "child_process";



const multiInput = multiInputer.default;

const port = process.env.PUBLIC_STOREFRONT_SVELTE_PORT;
const applicationId = process.env.APPLICATION_ID;

function readFile(path) {
	return new Promise((resolve, reject) => {
		fs.readFile(path, "utf8", function (err, data) {
			if (err) {
				reject(err);
			}
			resolve(data);
		});
	});
}

function saveFile(path, data) {
	return new Promise((resolve, reject) => {
		fs.writeFile(path, data, function (err) {
			if (err) {
				reject(err);
			}
			resolve(data);
		});
	});
}

function createConstantsFile() {
	return {
		async buildStart(source) {
			const jsPath = "./src/constants.js";
			const jsFile = await readFile(jsPath);

			const newFile = jsFile.replace(
				/(APP_NAME\s?=\s?")([^"]+)(")/m,
				`$1${applicationId}$3`
			);
			await saveFile(jsPath, newFile);
		},
	};
}

/* <-- */

function serve() {
	let server;

	function toExit() {
		if (server) server.kill(0);
	}

	return {
		writeBundle() {
			if (server) return;

			server = spawn(
				"sirv",
				["build", "--dev", "--cors", `--port ${port}`, "--no-clear"],

				{
					stdio: ["ignore", "inherit", "inherit"],
					shell: true,
				}
			);

			process.on("SIGTERM", toExit);
			process.on("exit", toExit);
		},
	};
}

// récupère dynamiquement le nom du package
const config = JSON.parse(
	await readFile('./package.json')
);
const name = config.name.replace(/[^\/]+\//, "");
const buildPath = `build/${name}`;


export default [
	{
		input: "./src/webComponents/*.js",
		output: {
			format: "esm",
			dir: buildPath
		},
		plugins: [
			createConstantsFile(),
			multiInput({ relative: "src/webComponents/" }),
			svelte({
				exclude: /\.wc\.svelte$/,
				compilerOptions: {
					dev: true,
					customElement: false,
					enableSourcemap: false,
				},
				emitCss: false,
			}),
			svelte({
				// compile only *.wc.svelte files as web components
				include: /\.wc\.svelte$/,
				compilerOptions: {
					dev: true,
					customElement: true,
					enableSourcemap: false,
				},
				emitCss: true,
			}),
			resolve(),
			serve(),
			summary(),
		],
		watch: {
			clearScreen: false,
			exclude: "src/constants.js",
		},
	},
	{
		input: "src/page/*.js",
		output: {
			format: "iife",
			dir: buildPath
		},
		plugins: [
			multiInput({ relative: "src/page/" }),
			resolve(),
	//		wrap('( () => {', '})()'),
			summary(),
		],
		watch: {
			clearScreen: false,
			exclude: "src/constants.js",
		},
	},
];
