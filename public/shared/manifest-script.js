module.exports = {
	scripts: [
		{
			id: "shared",
			name: "@APPLICATION_ID@-shared-config",
			description: "Lorem Ipsum",
			auto_uninstall: true,
			load_method: "default",
			location: "head",
			visibility: "all_pages", //storefront, all_pages, checkout, order_confirmation
			kind: "script_tag",
			consent_category: "essential", //essential, functional, analytics, targeting
			enabled: true,
			html: ` 
 <script>
		window.oxatis=window.oxatis||{};
		window.oxatis["@APPLICATION_ID@"] = {
			store_hash:"{{settings.store_hash}}",
			secure_url:"{{settings.secure_base_url}}",
			storefront_api_url: "@storefront_api_url@",
			storefront_api_token:"{{settings.storefront_api.token}}",
			currency_code: "{{currency_selector.active_currency_code}}",
			default_currency_code: "{{currency_selector.default_currency_code}}",
			decimal_places: "{{settings.money.decimal_places}}",
			currency_token: "{{settings.money.currency_token}}",
			thousands_token: "{{settings.money.thousands_token}}",
			decimal_token:"{{settings.money.decimal_token}}",
			currency_location : "{{settings.money.currency_location}}",
			template_file: "{{template_file}}",
			checkout_id: "{{checkout.id}}",
		};
</script>
`,
		},
	],
};
